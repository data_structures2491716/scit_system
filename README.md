**Type of Data structure used**- array


**Reasons**

•	Simplicity: It is easy to work with arrays to store data

•	Ease access of data: With an array, it easy to access individual student records using index, which is sufficient and direct.

•	Memory efficiency: Since each student structure contains only the necessary fields and there is no overhead with the dynamic memory

•	Sequential Access. The data in an array is sequentially stored in memory. Iterating, sorting and exporting student records is straight forward.

**YOUTUBE LINKS**

[Mayanja Pius](https://youtu.be/IIjT1aT-YjA)

[Amanyire Cindy](https://youtu.be/lUpWN3Cp9qs?si=ghpbWl8penuVaXEH)

[Katungye Brendan](https://youtu.be/uyJjRtLGl9E)

[Kalyango Timothy](https://youtu.be/_QVQD8svXxY?si=djMismOGGU-FQsTI)

[Muwanguzi Mercy](https://youtu.be/FS1p4XmNEU4)