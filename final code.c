#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STUDENTS 10000000
#define MAX_INPUT 256

typedef struct {
    char name[51];
    char dob[12];
    char reg_num[10];
    char course_code[7];
    float tuition;
} Student;

// Global array to store student records
Student students[MAX_STUDENTS];
int num_students = 0; // Number of students currently in the array

// Function prototypes
void createStudent();
void printStudent(Student s);
void updateStudent(Student students[], int *num_students, const char *regNumber);
void deleteStudent(Student students[], int *num_students, const char *regNumber);
int searchStudentByRegNumber(Student students[], int num_students, const char *regNumber);
void sortStudents(Student students[], int num_students, int field);
void exportStudentsToFile(const char* filename);

int main() {
    int choice;
    do {
        printf("\n=== Student Record Management System ===\n");
        printf("1. Create a New Student\n");
        printf("2. Read Student Record\n");
        printf("3. Update Student Record\n");
        printf("4. Delete Student from Records\n");
        printf("5. Search Student by Registration\n");
        printf("6. Sort Students\n");
        printf("7. Export Students to File\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);
        getchar(); // Consume newline
        
        switch(choice) {
            case 1:
                createStudent();
                break;
            case 2:
                printf("\n");
                printf("Enter registration number of the student to print: ");
                char regNumberToPrint[7];
                scanf("%6s", regNumberToPrint);
                getchar(); // Consume newline
                int index = searchStudentByRegNumber(students, num_students, regNumberToPrint);
                if (index != -1) {
                    printStudent(students[index]);
                }
                break;
                
            case 3:
                printf("\n");
                printf("Enter registration number of the student to update: ");
                char regNumberToUpdate[7];
                scanf("%6s", regNumberToUpdate);
                getchar(); // Consume newline
                updateStudent(students, &num_students, regNumberToUpdate);
                break;

            case 4:
                printf("\n");
                printf("Enter registration number of the student to delete: ");
                char regNumberToDelete[7];
                scanf("%6s", regNumberToDelete);
                getchar(); // Consume newline
                deleteStudent(students, &num_students, regNumberToDelete);
                break;

            case 5:
                printf("\n");
                printf("Enter registration number to search: ");
                char regNumber[7];
                scanf("%6s", regNumber); // Limit input to 6 characters to prevent buffer overflow
                getchar(); // Consume newline
                searchStudentByRegNumber(students, num_students, regNumber);
                break;
            case 6:
                printf("\n");
                printf("Sort students by:\n");
                printf("1. Name\n");
                printf("2. Registration Number\n");
                printf("Enter your choice: ");
                int sortField;
                scanf("%d", &sortField);
                getchar(); // Consume newline
                sortStudents(students, num_students, sortField);
                break;

            case 7:
                exportStudentsToFile("students.csv");
                printf("Students exported to file successfully.\n");
                break;
            case 8:
                printf("Exiting program.\n");
                break;
            default:
                printf("Invalid choice. Please enter a number from 1 to 8.\n");
        }
    } while(choice != 8);

    return 0;
}

void createStudent() {
    if (num_students >= MAX_STUDENTS) {
        printf("Maximum number of students reached.\n");
        return;
    }
    printf("\n");
    printf("Enter student name (max 50 characters: ");
    fgets(students[num_students].name, sizeof(students[num_students].name), stdin);
    students[num_students].name[strcspn(students[num_students].name, "\n")] = '\0';

    printf("Enter date of birth (YYYY-MM-DD): ");
    fgets(students[num_students].dob, sizeof(students[num_students].dob), stdin);
    students[num_students].dob[strcspn(students[num_students].dob, "\n")] = '\0';

    printf("Enter registration number (6 numeric characters only): ");
    fgets(students[num_students].reg_num, sizeof(students[num_students].reg_num), stdin);
    students[num_students].reg_num[strcspn(students[num_students].reg_num, "\n")] = '\0';

    printf("Enter program_code( only 4 charaters): ");
    fgets(students[num_students].course_code, sizeof(students[num_students].course_code), stdin);
    students[num_students].course_code[strcspn(students[num_students].course_code, "\n")] = '\0';

    printf("Enter annual tuition: ");
    char input[MAX_INPUT];
    fgets(input, MAX_INPUT, stdin);
    students[num_students].tuition = strtof(input, NULL);
    while (students[num_students].tuition <= 0) {
        printf("Error: Annual tuition can't zero. Enter again: ");
        fgets(input, MAX_INPUT, stdin); // read new input
        students[num_students].tuition = strtof(input, NULL);
    }
    num_students++; // Increment the number of students
}

void printStudent(Student s) {
    printf("\n");
    printf("Name: %s\n", s.name);
    printf("Date of Birth: %s\n", s.dob);
    printf("Registration Number: %s\n", s.reg_num);
    printf("Course Code: %s\n", s.course_code);
    printf("Annual Tuition: %.2f\n", s.tuition);
}

void updateStudent(Student students[], int *num_students, const char *regNumber) {
    // Flag to track if the student with the specified registration number is found
    int found = 0;

    // Iterate over the student records to find the student with the specified registration number
    for (int i = 0; i < *num_students; i++) {
        if (strcmp(students[i].reg_num, regNumber) == 0) {
            // Student found, so set the found flag to true
            found = 1;

            // Print student information
            printf("\nStudent found:\n");
            printStudent(students[i]);

            // Prompt for updated information
            printf("\nEnter updated information:\n");

            // Update student name
            printf("Enter student name: ");
            fgets(students[i].name, sizeof(students[i].name), stdin);
            students[i].name[strcspn(students[i].name, "\n")] = '\0';

            // Update date of birth
            printf("Enter date of birth (YYYY-MM-DD): ");
            fgets(students[i].dob, sizeof(students[i].dob), stdin);
            students[i].dob[strcspn(students[i].dob, "\n")] = '\0';

            // Update course code
            printf("Enter course code: ");
            fgets(students[i].course_code, sizeof(students[i].course_code), stdin);
            students[i].course_code[strcspn(students[i].course_code, "\n")] = '\0';

            // Update registration number
            printf("Enter registration number: ");
            fgets(students[i].reg_num, sizeof(students[i].reg_num), stdin);
            students[i].reg_num[strcspn(students[i].reg_num, "\n")] = '\0';

            // Update annual tuition
            printf("Enter annual tuition: ");
            char input[MAX_INPUT];
            fgets(input, MAX_INPUT, stdin);
            students[i].tuition = strtof(input, NULL);
            while (students[i].tuition <= 0) {
                printf("Error: Annual tuition cant't zero. Enter again: ");
                fgets(input, MAX_INPUT, stdin); // read new input
                students[i].tuition = strtof(input, NULL);
            }

            // Print confirmation message
            printf("\nStudent information updated successfully.\n");

            // Exit loop since the student has been found and updated
            break;
        }
    }

    // Check if the student with the specified registration number was not found
    if (!found) {
        printf("Student with registration number %s not found.\n", regNumber);
    }
}

void deleteStudent(Student students[], int *num_students, const char *regNumber) {
    int found = 0;
    for (int i = 0; i < *num_students; i++) {
        if (strcmp(students[i].reg_num, regNumber) == 0) {
            printf("\n");
            printf("Student found and deleted:\n");
            printf("Name: %s\n", students[i].name);
            printf("Date of Birth: %s\n", students[i].dob);
            printf("Registration Number: %s\n", students[i].reg_num);
            printf("Course Code: %s\n", students[i].course_code);
            printf("Annual Tuition: %.2f\n", students[i].tuition);
            
            // Shift all subsequent elements one position to the left
            for (int j = i; j < *num_students - 1; j++) {
                strcpy(students[j].name, students[j + 1].name);
                strcpy(students[j].dob, students[j + 1].dob);
                strcpy(students[j].reg_num, students[j + 1].reg_num);
                strcpy(students[j].course_code, students[j + 1].course_code);
                students[j].tuition = students[j + 1].tuition;
            }
            (*num_students)--; // Decrement the number of students
            found = 1;
            break;
        }
    }
    if (!found) {
        printf("Student with registration number %s not found.\n", regNumber);
    }
}

int searchStudentByRegNumber(Student students[], int num_students, const char *regNumber) {
    int found = 0;
    for (int i = 0; i < num_students; i++) {
        if (strcmp(students[i].reg_num, regNumber) == 0) {
            printf("\n");
            printf("Student found:\n");
            printf("Name: %s\n", students[i].name);
            printf("Date of Birth: %s\n", students[i].dob);
            printf("Registration Number: %s\n", students[i].reg_num);
            printf("Course Code: %s\n", students[i].course_code);
            printf("Annual Tuition: %.2f\n", students[i].tuition);
            found = 1;
            break;
        }
    }
    if (!found) {
        printf("Student with registration number %s not found.\n", regNumber);
    }
    return found;
}

void sortStudents(Student students[], int num_students, int field) {
    // Field 1: Sort by name, Field 2: Sort by registration number
    switch(field) {
        case 1:
            // Sort by name
            for (int i = 0; i < num_students - 1; i++) {
                for (int j = 0; j < num_students - i - 1; j++) {
                    if (strcmp(students[j].name, students[j + 1].name) > 0) {
                        // Swap
                        Student temp = students[j];
                        students[j] = students[j + 1];
                        students[j + 1] = temp;
                    }
                }
            }
            printf("Students sorted by name:\n");
            for (int i = 0; i < num_students; i++) {
                printf("%d. Name: %s\n", i + 1, students[i].name);
                printf("   Date of Birth: %s\n", students[i].dob);
                printf("   Registration Number: %s\n", students[i].reg_num);
                printf("   Course Code: %s\n", students[i].course_code);
                printf("   Annual Tuition: %.2f\n", students[i].tuition);
            }
            break;
            
        case 2:
            // Sort by registration number
            for (int i = 0; i < num_students - 1; i++) {
                for (int j = 0; j < num_students - i - 1; j++) {
                    if (strcmp(students[j].reg_num, students[j + 1].reg_num) > 0) {
                        // Swap
                        Student temp = students[j];
                        students[j] = students[j + 1];
                        students[j + 1] = temp;
                    }
                }
            }
            printf("Students sorted by registration number:\n");
            for (int i = 0; i < num_students; i++) {
                printf("%d. Registration Number: %s\n", i + 1, students[i].reg_num);
                printf("   Name: %s\n", students[i].name);
                printf("   Date of Birth: %s\n", students[i].dob);
                printf("   Course Code: %s\n", students[i].course_code);
                printf("   Annual Tuition: %.2f\n", students[i].tuition);
            }
            break;
            
        default:
            printf("Invalid field.\n");
    }
}

void exportStudentsToFile(const char* filename) {
    FILE *file = fopen(filename, "w"); // Open file in write mode
    
    if (file == NULL) {
        printf("Error opening file for writing.\n");
        return;
    }

    // Write header to the file
    fprintf(file, "Name,Date of Birth,Registration Number,Course Code,Annual Tuition\n");

    // Write student records to the file
    for (int i = 0; i < num_students; ++i) {
        fprintf(file, "%s,%s,%s,%s,%.2f\n", students[i].name, students[i].dob, students[i].reg_num, students[i].course_code, students[i].tuition);
    }

    fclose(file);
}

